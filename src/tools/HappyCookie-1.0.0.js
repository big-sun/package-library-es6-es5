class happyCookie {
  constructor() {
    this["_instruction"] = {
      "存cookie": `
        1.参数说明
          1.1cookieName:设置cookie名字 string
          1.2cookieValue:设置cookie的值 string
          1.3expiresObj:设置cookie的配置参数对象 null/{}/{timeScale:'秒/分/时 日/月/年',timeSize:number}
            1.3.1null:用户没有传入第三个参数，或者传入null。浏览器会在页面关闭时删除该cookie
            1.3.2{}:用户传入的是一个空对象。默认过期时间为15分钟
            1.3.3{timeScale:'时',timeSize:8}:过期时间为8小时
        2.方法名:setCookie(cookieName, cookieValue, expiresObj = null){}
      `,
      "取cookie": `
        1.参数说明
          1.1cookieName:获取的cookie名字 string
        2.方法名:getCookie(cookieName){}
      `,
      "删cookie": `
        1.参数说明
          1.1cookieName:删除的cookie名字  string
        2.方法名:delCookie(cookieName){}
      `,
      "删除全部cookie": `
        1.方法名:delAllCookie(){}
      `,
      "导出/引入格式": `
        1.导出格式
        let hc=new HappyCookie();
        export{
          hc
        }
        2.引入格式
        import happycookie,{hc} from '路径'
      `
    }
  }

  //这个函数就是使用说明
  instruction() {
    for (let [key, value] of Object.entries(this._instruction)) {
      console.log(key, value);
    }
  }

  /**
   * @param {*} cookieName:cookie的名字 string
   * @param {*} cookieValue:cookie的值 string
   * @param {*} expiresObj:不传/{}/{timeScale:'秒/分/时 日/月/年',timeSize:number}
   */

  setCookie(cookieName, cookieValue, expiresObj = null) {
    if (!expiresObj) {//用户没有传入第三个参数
      //不设置这个时间戳，浏览器会在页面关闭时即将删除该cookie
      document.cookie = `${cookieName}=${cookieValue}; path=/`;
    } else if (Object.keys(expiresObj).length == 0) {//输入的是一个空对象 设置默认过期时间:15分钟
      let newDate = new Date();//获取当前的时间对象
      let nowTimeStamp = newDate.getTime();//获取当前时间对象的时间戳
      nowTimeStamp += (1000 * 60 * 15);
      newDate.setTime(nowTimeStamp);
      let expires = "expires=" + newDate.toGMTString();
      document.cookie = `${cookieName}=${cookieValue};${expires}; path=/`;
    } else {
      let newDate = new Date();//获取当前的时间对象
      let nowTimeStamp = newDate.getTime();//获取当前时间对象的时间戳
      switch (expiresObj.timeScale) {//判断时间规格
        case '秒':
          nowTimeStamp += (1000 * expiresObj.timeSize)
          break;
        case '分':
          nowTimeStamp += (1000 * 60 * expiresObj.timeSize)
          break;
        case '时':
          nowTimeStamp += (1000 * 60 * 60 * expiresObj.timeSize)
          break;
        case '日':
          nowTimeStamp += (1000 * 60 * 60 * 24 * expiresObj.timeSize)
          break;
        case '月':
          nowTimeStamp += (1000 * 60 * 60 * 24 * 30 * expiresObj.timeSize)
          break;
        case '年':
          nowTimeStamp += (1000 * 60 * 60 * 24 * 30 * 12 * expiresObj.timeSize)
          break;

        default:
          break;
      }
      newDate.setTime(nowTimeStamp)
      let expires = "expires=" + newDate.toGMTString();
      document.cookie = `${cookieName}=${cookieValue};${expires}; path=/`;
    }
  }

  getCookie(cookieName) {
    let name = cookieName + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i].trim();
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }

  delCookie(cookieName) {
    document.cookie = cookieName + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
  }

  delAllCookie() {
    let keysArr = document.cookie.match(/[^ =;]+(?=\=)/g);
    if (keysArr && keysArr.length > 0) {
      keysArr.forEach(item => {
        this.delCookie(item)
      })
    }
  }
}

let hc = new happyCookie();

export {
  hc
}